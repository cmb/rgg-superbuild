include(rgg.bundle.common)
set(rgg_package "RGG_Suite_${rgg_version_major}.${rgg_version_minor}.${rgg_version_patch}")

superbuild_apple_create_app(
  "\${CMAKE_INSTALL_PREFIX}/${rgg_package}"
  "RGGNuclear.app"
  "${superbuild_install_location}/Applications/RGGNuclear.app/Contents/MacOS/RGGNuclear"
  CLEAN
  SEARCH_DIRECTORIES
    "${superbuild_install_location}/lib")

install(
  FILES       "${superbuild_install_location}/Applications/RGGNuclear.app/Contents/Resources/MacIcon.icns"
  DESTINATION "${rgg_package}/RGGNuclear.app/Contents/Resources"
  COMPONENT   superbuild)
install(
  FILES       "${superbuild_install_location}/Applications/RGGNuclear.app/Contents/Info.plist"
  DESTINATION "${rgg_package}/RGGNuclear.app/Contents"
  COMPONENT   superbuild)

foreach (qt5_plugin_path IN LISTS qt5_plugin_paths)
  get_filename_component(qt5_plugin_group "${qt5_plugin_path}" DIRECTORY)
  get_filename_component(qt5_plugin_group "${qt5_plugin_group}" NAME)

  superbuild_apple_install_module(
    "\${CMAKE_INSTALL_PREFIX}/${rgg_package}"
    "RGGNuclear.app"
    "${qt5_plugin_path}"
    "Contents/Plugins/${qt5_plugin_group}"
    SEARCH_DIRECTORIES  "${library_paths}")
endforeach ()

if (meshkit_enabled)
  foreach (meshkit_exe IN ITEMS coregen assygen)
    superbuild_apple_install_utility(
      "\${CMAKE_INSTALL_PREFIX}/${rgg_package}"
      "RGGNuclear.app"
      "${superbuild_install_location}/meshkit/bin/${meshkit_exe}"
      SEARCH_DIRECTORIES  "${superbuild_install_location}/meshkit/lib"
      FRAMEWORK_DEST      "Frameworks/meshkit"
      LIBRARY_DEST        "Libraries/meshkit")
  endforeach ()
endif ()
