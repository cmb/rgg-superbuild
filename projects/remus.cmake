superbuild_add_project(remus
  DEPENDS cxx11 boost zeromq
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DRemus_ENABLE_EXAMPLES:BOOL=OFF
    -DRemus_NO_SYSTEM_BOOST:BOOL=ON
    -DRemus_ENABLE_TESTING:BOOL=OFF)

if (WIN32)
  set(remus_dir "<INSTALL_DIR>/cmake/Remus")
else ()
  set(remus_dir "<INSTALL_DIR>/lib/cmake/Remus")
endif ()

superbuild_add_extra_cmake_args(
  -DRemus_DIR:PATH=${remus_dir})
