superbuild_set_revision(boost
  URL     "https://www.paraview.org/files/dependencies/boost_1_64_0.tar.bz2"
  URL_MD5 93eecce2abed9d2442c9676914709349)

superbuild_set_revision(hdf5
  URL     "https://www.paraview.org/files/dependencies/hdf5-1.10.3.tar.bz2"
  URL_MD5 56c5039103c51a40e493b43c504ce982)

superbuild_set_revision(zeromq
  GIT_REPOSITORY  "https://github.com/robertmaynard/zeromq4-x.git"
  GIT_TAG         "master")

# Use remus from Fri Sep 1 15:56:16 2017 +0000
superbuild_set_revision(remus
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/remus.git"
  GIT_TAG        85355887b0da9d8b81bf1de999ba3a0f3ea7eb80)

superbuild_set_revision(vtk
  GIT_REPOSITORY  "https://gitlab.kitware.com/vtk/vtk.git"
  GIT_TAG         v8.0.0)

superbuild_set_revision(eigen
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/eigen-eigen-034b6c3e1017.zip"
  URL_MD5 a2626b3815fa9b0982b4bd8d52583895)

superbuild_set_revision(moab
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/moab-6425a480ffe8e08b96453618fc5530e08e68ae8a.tar.bz2"
  URL_MD5 5946a405cddce972d54044fca6c87b11)

superbuild_set_revision(qttesting
  GIT_REPOSITORY  "https://gitlab.kitware.com/paraview/qttesting.git"
  GIT_TAG         master)

superbuild_set_revision(meshkit
  SOURCE_DIR "${CMAKE_CURRENT_LIST_DIR}/meshkit")

superbuild_set_selectable_source(rgg
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY "https://gitlab.kitware.com/cmb/rgg.git"
    GIT_TAG "master"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-rgg")
