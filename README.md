Superbuild for RGG Application.

# Requiremnts
* CMake version 3.9 or greater
* Qt related info
  * Have Qt 5.9 or greater installed on the system and make sure you set USE_SYSTEM_qt5 to on
  * In CMake: QT5_DIR needs to be set to \<Qt installation dir for your compiler\>/lib/cmake/Qt5
* This project has been tested on OSX 10.14 and Ubuntu 18.04.

# Building RGG using the SuperBuild Process

## Prepping the Git Repo

1. Clone the RGG SuperBuild Repo using `git clone https://gitlab.kitware.com/cmb/rgg-superbuild.git`
2. Using a shell in the cloned repository, run `git submodule update --init`

## Configuring the Build Using CMake

There are two possible methods you can use: CMake GUI or the ccmake command line tool

### Using the CMake GUI

1. Select the Source directory that contains the CMake SuperBuild Git Repo
2. Select the build directory to be used to hold the build.  Note that due to
   a bug in git this should not be under and git repository including the
   Source Directory.

### Using ccmake commandline tool

1. Make the directory you want to build in
2. cd into that directory
3. If you are building with ninja (as oppose to make) run
   `cmake -G Ninja <Path to your super build repo>`, else omit the `-G Ninja`

### Configuring the RGG SuperBuild

 * Turn `USE_SYSTEM_qt5` on
 * Tell CMake to configure
 * Check to see if the `Qt5_DIR` variable is set to the appropriate location -
   if is not then set it correctly
 * On Windows, the directory to the Qt5 libraries must be in the `PATH`
   environment variable in order to build.
 * Tell CMake to configure
 * Tell CMake to generate

## Building the RGG SuperBuild

* cd into the build directory
* run make or ninja - depending on which build system you previously selected.

## Building a RGG Installable Package
* cd into the build directory
* run ctest -R cpack

## Meshkit support
* Turn on `ENABLE_meshkit` at top level build directory and call make or ninja again.
