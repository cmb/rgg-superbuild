superbuild_add_project(cgm
  CMAKE_ARGS
    # Set link path on linux
    -DCMAKE_INSTALL_RPATH:PATH=$ORIGIN/../lib
    # Set link path on OSX
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DCGM_OCC:BOOL=OFF
    -DUSE_MPI:BOOL=OFF)

superbuild_add_extra_cmake_args(
  -DCGM_DIR:PATH=<INSTALL_DIR>/lib/cmake/CGM)
